package bank;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable {
	
	private int accountId;
	private int accountCreationYear;
	private double accountBalance;
	private String accountType;
	
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	
	public int getAccountId() {
		return accountId;
	}
	
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}
	
	public double getAccountBalance() {
		return accountBalance;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public void setAccountCreationDate() {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		accountCreationYear = calendar.get(Calendar.YEAR);
	}
	
	public String getAccountCreationDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public void setCreationYear(int accountCreationYear) {
		this.accountCreationYear = accountCreationYear;
	}
	
	public int getCreationYear() {
		return accountCreationYear;
	}
	
	public abstract void deposit(double sum);
	public abstract void withdraw();
	public abstract String toString();


}
