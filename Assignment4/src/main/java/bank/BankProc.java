package bank;

public interface BankProc {
	
	/**
	 * @pre person != null
	 * @post getSize() == getSize()@pre + 1
	 * @post personAccount.containsKey(person) == true
	 * @param person
	 */
	public void addPerson(Person person);
	
	
	/**
	 * @pre person != null
	 * @pre personAccount.containsKey(person) == true
	 * @post getSize() == getSize()@pre - 1
	 * @post personAccount.containsKey(person) == false
	 * @param person
	 */
	public void removePerson(Person person);
	
	
	/**
	 * @pre person != null && account != null
	 * @pre personAccount.containsKey(person) == true
	 * @post accountListSize() == accountListSize@pre + 1
	 * @param person
	 * @param account
	 */
	public void addAccount(Person person, Account account);
	
	
	/**
	 * @pre person != null && account != null
	 * @pre personAccount.containsKey(person) == true
	 * @post accountListSize() == accountListSize@pre - 1
	 * @param person
	 * @param account
	 */
	public void removeAccount(Person person, Account account);
	
	
	/**
	 * @pre person != null
	 * @pre personAccount.containsKey(person) == true
	 * @nochange
	 * @param person
	 * @param account
	 */
	public String generateReport(Person person);
	
	/**
	 * @pre person != null && account != null && sum >= 0
	 * @pre personAccount.containsKey(person) == true
	 * @post accountBalancePost = accountBalancePre + sum;
	 * @param person
	 * @param account
	 * @param sum
	 */
	public void deposit(Person person, Account account, double sum);
	
	/**
	 * @pre person != null && account != null && sum >= 0
	 * @pre personAccount.containsKey(person) == true
	 * @post accountBalancePost = accountBalancePre - sum;
	 * @param person
	 * @param account
	 * @param sum
	 */
	public void withdraw(Person person, Account account, double sum);
	
}
