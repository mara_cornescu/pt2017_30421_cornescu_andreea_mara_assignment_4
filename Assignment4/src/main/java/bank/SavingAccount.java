package bank;

import java.util.Random;

public class SavingAccount extends Account{

	private static final double INTEREST = 0.01; 
	private static final double MIN_DEPOSIT_SUM = 500;
	private int currentYear;

	public SavingAccount(int accountId) {
		setAccountId(accountId);
		setAccountCreationDate();
		setAccountType("Saving Account");
	}
	
	public SavingAccount(int accountId, int creationYear, double balance, String type) {
		setAccountId(accountId);
		setCreationYear(creationYear);
		setAccountBalance(balance);
		setAccountType("Saving Account");
	}
	
	public void deposit(double sum) {
		if(getAccountBalance() > 0) 
			throw new IllegalArgumentException("Sum cannot be added to saving account!");
		if(sum < MIN_DEPOSIT_SUM) 
			throw new IllegalArgumentException("Initial sum has to be greater than 500$!");
		setAccountBalance(sum);
		setChanged();
		notifyObservers("A new deposit was made");
	}
	
	
	public void setCurrentYear() {
		Random rand = new Random();
		currentYear = rand.nextInt((2025 - 2018) + 1) + 2018;
	}
	
	public int getCurrentYear() {
		return currentYear;
	}
	
	public void addInterest() {
		int currentAddedSum = (getCurrentYear() - getCreationYear()) * 12;
		setAccountBalance(getAccountBalance() + getAccountBalance() * INTEREST * currentAddedSum);
		setChanged();
		notifyObservers("Interest was added");
	}

	public void withdraw() {
		setAccountBalance(0);
		setChanged();
		notifyObservers("Withdrawal was made");
	}
	
	public String toString() {
		return "Saving Account: id " + getAccountId() +  ",  balance " + getAccountBalance() + ", creating date " + getAccountCreationDate()
		+ " creation year " + getCreationYear() + " current year " + getCurrentYear();
	}
		

}
