package bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @invariant isWellFormed()
 * @author Mara
 *
 */

public class Bank implements BankProc, Serializable {
	
	private  Map<Person, List<Account>> personAccount; 
	
	public Bank() {
		personAccount = new HashMap<Person, List<Account>>();
	}
	
	public void addPerson(Person person) {
		assert person != null;
		assert isWellFormed();
		int sizePre = personAccount.size();
		
		personAccount.put(person, null);
		System.out.println("Person added!");
		
		int sizePost = personAccount.size();
		assert sizePost == sizePre + 1;
		assert isWellFormed();
	}
	
	public void removePerson(Person person) {
		assert person != null;
		assert personAccount.containsKey(person);
		assert isWellFormed();
		int sizePre = personAccount.size();
		
		personAccount.remove(person);
		System.out.println("Person removed!");
		
		int sizePost = personAccount.size();
		assert sizePost == sizePre - 1;
		assert !personAccount.containsKey(person);
		assert isWellFormed();
	}
	
	public void addAccount(Person person, Account account) {
		assert person != null && account != null;
		assert isWellFormed();
		int sizePre = personAccount.keySet().size();
		
		if(personAccount.containsKey(person)) {
			if(personAccount.get(person) == null) {
				List<Account> accounts = new ArrayList<Account>();
				personAccount.put(person, accounts);
			}
			personAccount.get(person).add(account);
			account.addObserver(person);
			System.out.println("Account added!");
			
			int sizePost = personAccount.keySet().size();
			assert sizePre == sizePost;
		}
		else {
			List<Account> accounts = new ArrayList<Account>();
			accounts.add(account);
			personAccount.put(person, accounts);
			account.addObserver(person);
			System.out.println("Person and account added!");
			
			int sizePost = personAccount.keySet().size();
			assert sizePost == sizePre + 1;
		}
	
		assert isWellFormed();
	}
	
	public void removeAccount(Person person, Account account) {
		assert person != null && account != null;
		assert personAccount.containsKey(person);
		assert isWellFormed();
		int sizePre = personAccount.get(person).size();
		
		List<Account> accounts = personAccount.get(person);
			
		for(Account a: accounts) {
			if(account.equals(a)) {
				personAccount.get(person).remove(account);
				System.out.println("Account removed!");
			}
		}
		
		int sizePost = personAccount.get(person).size();
		assert sizePost == sizePre - 1;
		assert isWellFormed();
	}
	
	public void removeAccountById(Person person, int accountId) {
		assert person != null && accountId > 0;
		assert personAccount.containsKey(person);
		assert isWellFormed();
		int sizePre = personAccount.get(person).size();
		
		List<Account> accounts = personAccount.get(person);
			
		for(Account a: accounts) {
			if(accountId == a.getAccountId()) {
				personAccount.get(person).remove(a);
				break;
			}
		}
		
		int sizePost = personAccount.get(person).size();
		assert sizePost == sizePre - 1;
		assert isWellFormed();
	}
	
	
	public String generateReport(Person person) {
		assert person != null;
		assert personAccount.containsKey(person);
		assert isWellFormed();
		
		List<Account> accounts = personAccount.get(person);
		
		String report = "Person " + person.getPersonName() + ", age " + person.getPersonAge() + ", address " + person.getPersonAddress() 
		+ ", email " + person.getPersonEmail() + ", has the following accounts: \n";
		
		for(Account a: accounts) {
			if(a instanceof SavingAccount) {
				report += "Saving ";
			}
			if(a instanceof SpendingAccount) {
				report += "Spending ";
			}
			report += "account: id " + a.getAccountId() + ", created on " + a.getAccountCreationDate() + ", having a balance of " + a.getAccountBalance() + " \n";
		}
		
		assert isWellFormed();
		return report;
	}
	
	public void deposit(Person person, Account account, double sum) {
		assert person != null && account != null && sum >= 0;
		assert personAccount.containsKey(person);
		assert isWellFormed();
		double accountBalancePre = account.getAccountBalance();
		
		if(account instanceof SavingAccount) {
			SavingAccount savings = (SavingAccount) account;
			savings.deposit(sum);
			System.out.println("Sum deposited in saving account!");
		}
		if(account instanceof SpendingAccount) {
			SpendingAccount spendings = (SpendingAccount) account;
			spendings.deposit(sum);
			System.out.println("Sum deposited in spending account!");
		}
		
		double accountBalancePost = account.getAccountBalance();
		assert accountBalancePost == accountBalancePre + sum;
		assert isWellFormed();
	}
	
	public void withdraw(Person person, Account account, double sum) {
		assert person != null && account != null && sum >= 0;
		assert personAccount.containsKey(person);
		assert isWellFormed();
		double accountBalancePre = account.getAccountBalance();
		
		if(account instanceof SavingAccount) {
			SavingAccount savings = (SavingAccount) account;
			savings.withdraw();
			
			System.out.println("Sum withdrawn from saving account!");
			
			double accountBalancePost = savings.getAccountBalance();
			assert accountBalancePost == 0;
		}
		if(account instanceof SpendingAccount) {
			SpendingAccount spendings = (SpendingAccount) account;
			spendings.setWithdrawSum(sum);
			spendings.withdraw();
			
			System.out.println("Sum withdrawn from spending account!");
			
			double accountBalancePost = spendings.getAccountBalance();
			assert accountBalancePost == accountBalancePre - sum;
		}
		
		assert isWellFormed();
	}
	
	/*
	 * Bank invariant. Check is there is a null person in the map.
	 */
	protected boolean isWellFormed() {
		Set<Person> persons = personAccount.keySet();
		for(Person p: persons) {
			if(p == null) {
				return false;
			}
		}
		return true;
	}
	
	
	public Map<Person, List<Account>> getMap() {
		return personAccount;
	}
	
	public List<Person> getPersons() {
		List<Person> persons = new ArrayList<Person>(personAccount.keySet());
		return persons;
	}
	
	public List<Account> getAccounts(Person person) {
		List<Account> accounts = personAccount.get(person);
		return accounts;
	}
	
	public Account getPersonAccount(Person person, int accountId) {
		
		List<Account> accounts = personAccount.get(person);
		
		for(Account a: accounts) {
			if(accountId == a.getAccountId()) {
				return a;
			}
		}
		return null;
	}
}
