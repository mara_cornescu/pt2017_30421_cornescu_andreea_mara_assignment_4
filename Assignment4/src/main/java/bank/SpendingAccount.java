package bank;


public class SpendingAccount extends Account{

	public double withdrawSum;
	
	public SpendingAccount(int accountId) {
		setAccountId(accountId);
		setAccountCreationDate();
		setAccountType("Spending Account");
	}
	
	public SpendingAccount(int accountId, int creationYear, double balance, String type) {
		setAccountId(accountId);
		setCreationYear(creationYear);
		setAccountBalance(balance);
		setAccountType("Saving Account");
	}
	
	public void deposit(double sum) {
		setAccountBalance(getAccountBalance() + sum);
		setChanged();
		notifyObservers("A new deposit was made");
	}
	
	public void setWithdrawSum(double withdrawSum) {
		this.withdrawSum = withdrawSum;
	}
	
	public double getWithdrawSum() {
		return withdrawSum;
	}


	public void withdraw(){
		if(getWithdrawSum() > getAccountBalance()) 
			throw new IllegalStateException("Insufficient founds!");
			
		setAccountBalance(getAccountBalance() - getWithdrawSum());
		setChanged();
		notifyObservers("Withdrawal was made");
	}


	public String toString() {
		return "Spending Account: id " + getAccountId() +  ",  balance " + getAccountBalance() + ", creating date " + getAccountCreationDate();
	}

}
