package bank;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Observer, Serializable {
	
	private String personName;
	private String personAddress;
	private String personEmail;
	private int personAge;
	
	public Person(String personName, String personAddress, String personEmail, int personAge) {
		
		setPersonName(personName);
		setPersonAddress(personAddress);
		setPersonEmail(personEmail);
		setPersonAge(personAge);
		
	}
	
	public String getPersonName() {
		return personName;
	}
	
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
	public String getPersonAddress() {
		return personAddress;
	}
	
	public void setPersonAddress(String personAddress) {
		this.personAddress = personAddress;
	}
	
	public String getPersonEmail() {
		return personEmail;
	}
	
	public void setPersonEmail(String personEmail) {
		this.personEmail = personEmail;
	}
	
	public int getPersonAge() {
		return personAge;
	}
	
	public void setPersonAge(int personAge) {
		this.personAge = personAge;
	}
	
    @Override
    public int hashCode(){
       int result = 0;
       result = 31 * result + personAge;
       result = 31 * result + (personName !=null ? personName.hashCode() : 0);
       result = 31 * result + (personAddress  !=null ? personAddress.hashCode() : 0);
       result = 31 * result + (personEmail  !=null ? personEmail.hashCode() : 0);
      
       return result;
   }
    
    @Override
    public boolean equals(Object other){
        if(this == other) return true;
       
        if(other == null || (this.getClass() != other.getClass())){
            return false;
        }
       
        Person guest = (Person) other;
        return (this.personName != null && personName.equals(guest.personName)) &&
        		(this.personAge == guest.personAge) &&
        		(this.personEmail != null && personEmail.equals(guest.personEmail)) &&
        		(this.personAddress != null && personAddress.equals(guest.personAddress));
    }
	
	public String toString() {
		return "Person: " + getPersonName() + "age " + getPersonAge() + ", address " + getPersonAddress() + ", email " + getPersonEmail();
	}

	public void update(Observable arg0, Object arg1) {
		
		if(arg0 instanceof SavingAccount)
			System.out.println(arg1 + " in SavingAccount " + ((SavingAccount)arg0).getAccountId() + "!");
		else
			System.out.println(arg1 + " in SpendingAccount " + ((SpendingAccount)arg0).getAccountId() + "!");
		
	}

}
