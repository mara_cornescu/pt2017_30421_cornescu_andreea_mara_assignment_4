package gui;

import java.awt.GridLayout;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bank.Bank;


public class ViewPerson extends JFrame {
	
	private JPanel panelFin;
	private JLabel nameLabel, ageLabel, emailLabel, addressLabel;
	private JTextField nameTextField, ageTextField, emailTextField, addressTextField;
	private JButton addPerson;
	
	public ViewPerson(final Bank bank) {
		
		super("Persons");
		setLayout(new GridLayout());
		setSize(290,190);
		
		setResizable(false);
		
		panelFin = new JPanel();
		
		nameLabel = new JLabel("  Name   ");
		nameTextField = new JTextField(20);
		ageLabel = new JLabel("    Age     ");
		ageTextField = new JTextField(20);
		emailLabel = new JLabel("  E-mail  ");
		emailTextField = new JTextField(20);
		addressLabel = new JLabel("Address");
		addressTextField = new JTextField(20);
		
		addPerson = new JButton("Add Person");
		
		panelFin.add(nameLabel);
		panelFin.add(nameTextField);
		panelFin.add(ageLabel);
		panelFin.add(ageTextField);
		panelFin.add(emailLabel);
		panelFin.add(emailTextField);
		panelFin.add(addressLabel);
		panelFin.add(addressTextField);
		
		panelFin.add(addPerson);
		
		add(panelFin);
		
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	public String getPersonName() {
		return nameTextField.getText();
	}
	
	public int getPersonAge() {
		return Integer.parseInt(ageTextField.getText());
	}
	
	public String getPersonMail() {
		return emailTextField.getText();
	}
	
	public String getPersonAddress() {
		return addressTextField.getText();
	}
	
	public void addPerson(ActionListener listenForAddPersonButton) {
		addPerson.addActionListener(listenForAddPersonButton);
	}
	
	 void displayErrorMessage(String errorMessage) {
		 JOptionPane.showMessageDialog(this, errorMessage);
	 }


}
