package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;


public class View extends JFrame {
	
	private JPanel panelFin, panelButtons, panelPerson, panelAccount;
	private JButton addNewPersonButton, addNewAccountButton, depositButton, withdrawButton, refreshButton, deletePerson, deleteAccount, createReport;
	private JTable personTable, accountTable;
	private JScrollPane personPane, accountPane;
	private JLabel sumLabel;
	private JTextField sumTextField;
	
	public View() {
		
		super("Bank");
		setLayout(new GridLayout());
		setSize(1050,720);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		panelFin = new JPanel();
		panelButtons = new JPanel();
		panelPerson = new JPanel();
		panelAccount = new JPanel();
		
		addNewPersonButton = new JButton("Add Person");
		addNewAccountButton = new JButton("Add Account");
		deletePerson = new JButton("Delete Person");
		deleteAccount = new JButton("Delete Account");
		depositButton = new JButton("Deposit Money");
		withdrawButton = new JButton("Withdraw Money");
		refreshButton = new JButton("Refresh");
		createReport = new JButton("Generate Report");
		
		sumLabel = new JLabel("Sum ");
		sumTextField = new JTextField(5);
		
		panelButtons.add(addNewPersonButton);
		panelButtons.add(deletePerson);
		panelButtons.add(addNewAccountButton);
		panelButtons.add(deleteAccount);
		panelButtons.add(sumLabel);
		panelButtons.add(sumTextField);
		panelButtons.add(depositButton);
		panelButtons.add(withdrawButton);
		panelButtons.add(createReport);
		panelButtons.add(refreshButton);
		panelButtons.setBackground(Color.lightGray);
		
		personTable = new JTable();
		personTable.setPreferredSize(new Dimension(400, 500));
		personTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		personPane = new JScrollPane(personTable);
		personPane.setPreferredSize(new Dimension(800, 300));
		
		accountTable = new JTable();
		accountTable.setPreferredSize(new Dimension(400, 500));
		accountTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		accountPane = new JScrollPane(accountTable);
		accountPane.setPreferredSize(new Dimension(800, 300));
		
		panelFin.add(panelButtons);
		panelPerson.add(personPane);
		panelFin.add(panelPerson);
		panelAccount.add(accountPane);
		panelFin.add(panelAccount);
		add(panelFin);
		
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
	
	public double getSum() {
		return Double.parseDouble(sumTextField.getText());
	}
	
	public void addNewPersonListener(ActionListener listen) {
		addNewPersonButton.addActionListener(listen);
	}
	
	public void deletePersonListener(ActionListener listen) {
		deletePerson.addActionListener(listen);
	}
	
	public void addNewAccountListener(ActionListener listen) {
		addNewAccountButton.addActionListener(listen);
	}
	
	public void deleteAccountListener(ActionListener listen) {
		deleteAccount.addActionListener(listen);
	}
	
	public void depositListener(ActionListener listen) {
		depositButton.addActionListener(listen);
	}
	
	public void withdrawListener(ActionListener listen) {
		withdrawButton.addActionListener(listen);
	}
	
	public void reportListener(ActionListener listen) {
		createReport.addActionListener(listen);
	}
	
	public void setPersonTable(JTable table) {
		personTable = table;
	}
	
	public void setAccountTable(JTable table) {
		accountTable = table;
	}
	
	public void refreshListener(ActionListener listen) {
		refreshButton.addActionListener(listen);
	}	
	
	public void setMouseListener(MouseListener mouse) {
		depositButton.addMouseListener(mouse);
	}
	
	 void displayErrorMessage(String errorMessage) {
		 JOptionPane.showMessageDialog(this, errorMessage);
	 }
	
	public void getRefresh(JTable newTable) {
		newTable.setPreferredSize(new Dimension(800, 500));
		newTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane newPane = new JScrollPane(newTable);
		newPane.setPreferredSize(new Dimension(800, 200));
		
		panelPerson.removeAll();
		panelPerson.add(newPane);
		panelPerson.revalidate();
		panelPerson.repaint();
	}
	
	public void getRefreshAccount(JTable newTable) {
		newTable.setPreferredSize(new Dimension(800, 500));
		newTable.setBorder(BorderFactory.createLineBorder(Color.black, 1));
		JScrollPane newPane = new JScrollPane(newTable);
		newPane.setPreferredSize(new Dimension(800, 200));
		
		panelAccount.removeAll();
		panelAccount.add(newPane);
		panelAccount.revalidate();
		panelAccount.repaint();
	}
}
