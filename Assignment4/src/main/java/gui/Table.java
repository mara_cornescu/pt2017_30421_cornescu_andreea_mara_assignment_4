package gui;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;

import bank.Account;
import bank.Person;

public class Table {
	
	public static JTable createTable(List<Person> persons) {
		
		Object[][] resultData = new Object[persons.size()][4];
		String[] resultColumnName = new String[4];
		int i = 0;
		
		for(Person p: persons) {
			int j = 0;
			for (Field field : p.getClass().getDeclaredFields()) {
				field.setAccessible(true); 
				Object value;
				try {
					value = field.get(p);
					resultData[i][j] = value;
					resultColumnName[j] = field.getName();

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				++j;
			}
			++i;
		}
		
		return new JTable(resultData, resultColumnName);	
	}
	
	
	public static JTable createAccountTable(List<Account> accounts) {
		
		Object[][] resultData = new Object[accounts.size()][4];
		String[] resultColumnName = new String[4];
		int i = 0;
		
		for(Account a: accounts) {
			int j = 0;
			for (Field field : a.getClass().getSuperclass().getDeclaredFields()) {
				field.setAccessible(true); 
				Object value;
				try {
					value = field.get(a);
					resultData[i][j] = value;
					resultColumnName[j] = field.getName();

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				++j;
			}
			++i;
		}
		
		return new JTable(resultData, resultColumnName);	
	}
}
