package gui;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bank.Bank;

public class ViewAccount extends JFrame {

	private JFrame accountFrame;
	private JPanel panelFin;
	private JLabel idLabel;
	private JTextField idTextField;
	private JButton addSavingAccountButton, addSpendingAccountButton;
	
	public ViewAccount() {
			
		super("Account");
		setLayout(new GridLayout());
		setSize(200,150);
		setResizable(false);
		
		panelFin = new JPanel();
		
		idLabel = new JLabel("ID ");
		idTextField = new JTextField(5);	
		
		addSavingAccountButton = new JButton("Saving Account");
		addSpendingAccountButton = new JButton("Spending Account");
			
		panelFin.add(idLabel);
		panelFin.add(idTextField);
		
		panelFin.add(addSavingAccountButton);
		panelFin.add(addSpendingAccountButton);
	
		add(panelFin);
		
		setLocationRelativeTo(null);
		setVisible(true);
			
	}
	
	public int getID() {
		return Integer.parseInt(idTextField.getText());
	}
	
	public void addSpendingListener(ActionListener listen) {
		addSpendingAccountButton.addActionListener(listen);
	}
	
	public void addSavingListener(ActionListener listen) {
		addSavingAccountButton.addActionListener(listen);
	}
}
