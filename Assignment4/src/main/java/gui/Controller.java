package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

import PT2017.Assignment4.Assignment4.App;
import bank.Account;
import bank.Bank;
import bank.Person;
import bank.SavingAccount;
import bank.SpendingAccount;
import gui.View;
import gui.ViewPerson;
import gui.ViewAccount;

public class Controller {
	
	private View guiView;
	private ViewPerson guiPerson;
	private ViewAccount guiAccount;
	private Bank bank; 
	private JTable newTable, newTableAccount;
	
	public Controller(final View guiView, final Bank bank)  {
		this.guiView = guiView;
		this.bank = bank;
		
		this.guiView.addNewPersonListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				guiPerson = new ViewPerson(bank);
				
				guiPerson.addPerson(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						
						try {
							
							String personName;
							String personAddress;
							String personEmail;
							int personAge;
							
							personName = guiPerson.getPersonName();
							personAddress = guiPerson.getPersonAddress();
							personEmail = guiPerson.getPersonMail();
							personAge = guiPerson.getPersonAge();
							
							Person person = new Person(personName, personAddress, personEmail, personAge);
							bank.addPerson(person);
							App.serialize(bank);
							
						} catch(Exception ex) {
							guiPerson.displayErrorMessage("Wrong data entered!");
						}	
					}
				});
			}
		});
		
		
		this.guiView.setPersonTable(Table.createTable(bank.getPersons()));

		this.guiView.refreshListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				newTable = Table.createTable(bank.getPersons());
				guiView.getRefresh(newTable);
				newTable.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						try {
						guiView.setAccountTable(Table.createAccountTable(bank.getAccounts(getPerson())));
						newTableAccount = Table.createAccountTable(bank.getAccounts(getPerson()));
						guiView.getRefreshAccount(newTableAccount);
						}
						catch(Exception ex) {
							newTableAccount = new JTable();
							guiView.getRefreshAccount(newTableAccount);
						}
					}
				});
			}
		});
	
		this.guiView.addNewAccountListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guiAccount = new ViewAccount();
				
				guiAccount.addSavingListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							int accountId;
							accountId = guiAccount.getID();
							
							Account account = new SavingAccount(accountId);
							Person person = getPerson();
							
							bank.addAccount(person, account);
							App.serialize(bank);
							
						} catch(Exception ex) {
							
						}	
					}
				});
				
				guiAccount.addSpendingListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							int accountId;
							accountId = guiAccount.getID();
							
							Account account = new SpendingAccount(accountId);
							Person person = getPerson();
							
							bank.addAccount(person, account);
							App.serialize(bank);
							
						} catch(Exception ex) {
							
						}	
					}
				});
			}	
		});
		
		
		this.guiView.deleteAccountListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Person person = getPerson();
					//Account account = getAccount();
					int accountId = getAccountId();
				
					bank.removeAccountById(person, accountId);
					App.serialize(bank);
				} catch(Exception ex) {
					
				}
			}
		});
		
		this.guiView.deletePersonListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Person person = getPerson();
					bank.removePerson(person);
					App.serialize(bank);
				} catch(Exception ex) {
					
				}
			}
		});
		
		this.guiView.depositListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Person person = getPerson();
					Account account = bank.getPersonAccount(person, getAccountId());
					double sum = guiView.getSum();
				
					bank.deposit(person, account, sum);
					App.serialize(bank); 
				} catch(Exception ex) {
					if(guiView.getSum() < 500) {
						guiView.displayErrorMessage("Sum has to be greater that 500$!");
					}
					else {
						guiView.displayErrorMessage("Cannot add another sum to the saving account!");
					}
				}
			}
		});
		
		this.guiView.withdrawListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Person person = getPerson();
					Account account = bank.getPersonAccount(person, getAccountId());
					double sum = guiView.getSum();
				
					bank.withdraw(person, account, sum);
					App.serialize(bank);
				} catch(Exception ex) {
					guiView.displayErrorMessage("There are not enough money!");
				}
			}
		});
		
		this.guiView.reportListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					JFrame outputFrame = new JFrame("Report");
					JTextArea finalOutput = new JTextArea();
					JScrollPane scrollPane = new JScrollPane(finalOutput);
					scrollPane.setBounds(5, 5, 300, 300);
					outputFrame.add(scrollPane);
		
					finalOutput.append(bank.generateReport(getPerson()));
				
					outputFrame.setSize(800, 200);
					outputFrame.setResizable(false);
					outputFrame.setVisible(true);
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
	}
	
	
	public Person getPerson() {
		String name = (String) newTable.getValueAt(newTable.getSelectedRow(), 0);
		String address = (String) newTable.getValueAt(newTable.getSelectedRow(), 1);
		String email = (String) newTable.getValueAt(newTable.getSelectedRow(), 2);
		int age = (Integer) newTable.getValueAt(newTable.getSelectedRow(), 3);
		Person person = new Person(name, address, email, age);
		return person;
	}
	
	public int getAccountId() {
		int id = (Integer) newTableAccount.getValueAt(newTableAccount.getSelectedRow(), 0);
		return id;
	}
}


