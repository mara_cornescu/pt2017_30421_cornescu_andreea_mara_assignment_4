package PT2017.Assignment4.Assignment4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import bank.Account;
import bank.Bank;
import bank.Person;
import bank.SavingAccount;
import bank.SpendingAccount;
import gui.View;
import gui.ViewAccount;
import gui.ViewPerson;
import gui.Controller;


public class App 
{
	public static void serialize(Bank bank) {
		try {
	         FileOutputStream fileOut = new FileOutputStream("E:\\PT\\PT2017_30421_Cornescu_Andreea_Mara_Assignment_4\\Assignment4\\bank.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(bank);
	         out.close();
	         fileOut.close();
	         System.out.printf("Serialized data is saved in Assignment4\\bank.ser");
	      }catch(IOException i) {
	         i.printStackTrace();
	      }
	}
	
	public static Bank deserialize() {
		try {
	        FileInputStream fileIn = new FileInputStream("E:\\PT\\PT2017_30421_Cornescu_Andreea_Mara_Assignment_4\\Assignment4\\bank.ser");
	        ObjectInputStream in = new ObjectInputStream(fileIn);
	        Bank bank = (Bank) in.readObject();
	        in.close();
	        fileIn.close();
	        return bank;
	      }catch(IOException i) {
	         i.printStackTrace();
	      }catch(ClassNotFoundException c) {
	         System.out.println("Bank class not found");
	         c.printStackTrace();
	      }
		return null;
	}
	
	
    public static void main( String[] args )
    {
    	Bank bank = deserialize();
    	View view = new View();
    	Controller controller = new Controller(view, bank);
    	
		/*Bank bank = new Bank();
    	Person p = new Person("Bogdan Carcu", "Zorilor", "carcu.bogdan@yahoo.com", 21);
    	Person p1 = new Person("Negrea Denisa", "Observator", "negrea.denisa@gamil.com", 20);
		Person p2 = new Person("Cornescu Mara", "Zorilor", "mara.cornescu@gmail.com", 21);
    	bank.addPerson(p);
    	bank.addPerson(p1);
    	bank.addPerson(p2);
    	Account a1 = new SpendingAccount(1);
    	Account a2 = new SavingAccount(2);
		Account a3 = new SpendingAccount(3);
		Account a4 = new SpendingAccount(4);
    	Account a5 = new SavingAccount(5);
		Account a6 = new SpendingAccount(6);
    	bank.addAccount(p, a1);
    	bank.deposit(p, a1, 500);
   
    	bank.addAccount(p, a2);
    	bank.deposit(p, a2, 1000);
    	
    	bank.addAccount(p1, a3);
    	bank.deposit(p, a3, 200);
    	
    	bank.addAccount(p1, a4);
    	bank.deposit(p, a4, 400);
   
    	bank.addAccount(p1, a5);
    	bank.deposit(p, a5, 900);
    	
    	bank.addAccount(p2, a6);
    	bank.deposit(p, a6, 700);
    	
    	serialize(bank);*/
    	
    }
}
